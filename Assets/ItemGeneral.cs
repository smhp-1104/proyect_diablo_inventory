﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemGeneral : MonoBehaviour
{
    // Start is called before the first frame update
    private Vector3 mousePosition;
    public float moveSpeed;
    public bool placed;
    public bool pressed;

    void Start()
    {
        placed = false;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Slot"))
        {

            placed = true;
            


        }
    }

    // Update is called once per frame
    void Update()
    {


        if ( placed == false)
        {
            mousePosition = Input.mousePosition;
            mousePosition = Camera.main.ScreenToWorldPoint(mousePosition);
            transform.position = Vector2.Lerp(transform.position, mousePosition, moveSpeed);
        }


    }



    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Slot") && pressed == true)
        {

            placed = true;
            


        }
    }

    private void OnMouseDown()
    {
        pressed = true;
    }

    private void OnMouseUp()
    {
        pressed = false;
    }



}
