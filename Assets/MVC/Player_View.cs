﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_View : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Player_Model player_Model = GetComponent<Player_Model>();
        player_Model.rb2d.velocity = new Vector2(Input.GetAxis("Horizontal") * player_Model.speed, Input.GetAxis("Vertical") * player_Model.speed);
    }
}
