﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slot_View : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Slot_Model slot_Model = GetComponent<Slot_Model>();
        slot_Model.rend.material.color = Color.magenta;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Slot_Model slot_Model = GetComponent<Slot_Model>();

        if (collision.gameObject.CompareTag("Item") && slot_Model.pressed == false)
        {

            slot_Model.rend.material.color = Color.green;

        }



    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        Slot_Model slot_Model = GetComponent<Slot_Model>();

        if (collision.gameObject.CompareTag("Item") && slot_Model.pressed == true)
        {

            slot_Model.rend.material.color = Color.red;
            slot_Model.used = true;
            slot_Model.boxi.enabled = false;

        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        Slot_Model slot_Model = GetComponent<Slot_Model>();

        if (collision.gameObject.CompareTag("Item"))
        {

            slot_Model.rend.material.color = Color.magenta;

        }
    }
}
