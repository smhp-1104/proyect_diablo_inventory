﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Itemcito_View : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Itemcito_Model itemcito_Model = GetComponent<Itemcito_Model>();

        if (collision.gameObject.CompareTag("Player"))
        {
            GameObject temporalbullet = Instantiate(itemcito_Model.bulletslot);
            temporalbullet.transform.position = itemcito_Model.spawnslot.transform.position;
            Destroy(gameObject);
        }
    }
}
