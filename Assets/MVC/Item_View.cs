﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item_View : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Item_Model item_Model = GetComponent<Item_Model>();

        if (item_Model.placed == false)
        {
            item_Model.mousePosition = Input.mousePosition;
            item_Model.mousePosition = Camera.main.ScreenToWorldPoint(item_Model.mousePosition);
            transform.position = Vector2.Lerp(transform.position, item_Model.mousePosition, item_Model.moveSpeed);
        }
    }
}
