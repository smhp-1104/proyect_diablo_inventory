﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item_Controller : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Item_Model item_Model = GetComponent<Item_Model>();
        item_Model.placed = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        Item_Model item_Model = GetComponent<Item_Model>();

        if (collision.gameObject.CompareTag("Slot") && item_Model.pressed == true)
        {

            item_Model.placed = true;



        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Item_Model item_Model = GetComponent<Item_Model>();

        if (collision.gameObject.CompareTag("Slot"))
        {

            item_Model.placed = true;



        }
    }

    private void OnMouseDown()
    {
        Item_Model item_Model = GetComponent<Item_Model>();

        item_Model.pressed = true;
    }

    private void OnMouseUp()
    {
        Item_Model item_Model = GetComponent<Item_Model>();

        item_Model.pressed = false;
    }
}
