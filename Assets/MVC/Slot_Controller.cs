﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slot_Controller : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

        Slot_Model slot_Model = GetComponent<Slot_Model>();

        slot_Model.rend = GetComponent<Renderer>();
        slot_Model.boxi = GetComponent<BoxCollider2D>();
        slot_Model.used = false;
    }

    // Update is called once per frame
    void Update()
    {
        Slot_Model slot_Model = GetComponent<Slot_Model>();

        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            slot_Model.pressed = true;
        }

        if (Input.GetKeyUp(KeyCode.Mouse0))
        {
            slot_Model.pressed = false;
        }
    }
}
